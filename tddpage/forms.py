from django import forms
from .models import Greeting


class GreetingForm(forms.ModelForm):
    class Meta:
        model = Greeting
        fields = [

            'greet'

        ]

        widgets = {

            'greet': forms.TextInput(attrs={"class":"form-control"}),

        }
