from django.db import models


class Greeting(models.Model):

    greet = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)
