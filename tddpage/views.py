from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from .models import Greeting
from . import forms



def tdd(request):
    if request.method == "POST" :
        form = forms.GreetingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('test:tdd')


    else:
        form = forms.GreetingForm()
    return render (request, 'tdd.html', {"form":form, "list_greeting": Greeting.objects.all().order_by("-date")})
