from django.test import TestCase
from django.test import SimpleTestCase
from tddpage.forms import GreetingForm
from tddpage.models import Greeting
from django.test import Client
from django.urls import reverse
from tddpage.models import Greeting
from django.urls import reverse, resolve
from tddpage.views import tdd
import json
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from tddpage.models import Greeting
import time
from selenium.webdriver.chrome.options import Options
from . import forms


class TestFunctionalPage(StaticLiveServerTestCase):

    def setUp(self):

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestFunctionalPage, self).setUp()

    def tearDown(self):

        self.browser.quit()
        super(TestFunctionalPage, self).tearDown()


    def test_element_in_project(self):
        self.browser.get(self.live_server_url)


        alert = self.browser.find_element_by_class_name("box")
        self.assertEquals(

                alert.find_element_by_tag_name('h5').text,
                'Halo, Apa Kabar'

        )
        time.sleep(10)


    def test_response(self):

        project1 = Greeting.objects.create(

            greet = "Saya Baik"

        )
        self.browser.get(self.live_server_url)
        alert = self.browser.find_element_by_class_name("kotak")

        self.assertNotEquals(

            alert.find_element_by_tag_name('h5').text,
            'Saya Baik'
        )



class UnitTest(TestCase):

    def test_form_valid(self):

        form = GreetingForm(data = {
        'greet': 'Im Fine'
        } )

        self.assertTrue(form.is_valid())

    def test_no_data(self):
        form = GreetingForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)

    def setUp(self):
        self.project = Greeting.objects.create(
            greet='Im fine'
        )
        self.project1 = Greeting.objects.create(
            greet='Im fine'
        )

        self.client = Client()
        self.tdd_url = reverse('test:tdd')

    def test_tdd_GET(self):
        response = self.client.get(self.tdd_url, { "form":'greet'})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tdd.html')

    def test_tdd_POST_greet(self):
        greet_data = {
        'greet':'halo'
        }
        form = forms.GreetingForm(data = greet_data)
        self.assertTrue(form.is_valid())
        request = self.client.post(self.tdd_url, data= greet_data)
        self.assertEquals(request.status_code, 302)


        response = self.client.get(self.tdd_url)
        self.assertEquals(response.status_code,200)

    def test_project(self):
        self.assertNotEqual(self.project, self.project1 )

    def test_tdd_url_is_resolved(self):
        url = reverse('test:tdd')
        self.assertEquals(resolve(url).func , tdd)
